// WAPTP the product of odd digits in a given number  (256985)
//
class P8{
	public static void main(String[] args){
		int num = 256985;
		int rem = 0;
		int pro = 1;
		while (num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==1){
				pro = pro*rem;
			}
		}
		System.out.println(pro);
	}
}


