//WAPTP the digits from the given number which not divisible by 2
// (input => 2569185)
//
class P1{
	public static void main(String[] args){
		int num = 2569185;
		int rem = 0;
		while (num>0){
			rem = num%10;
			num = num/10;
			if(rem%2==1){
				System.out.print(rem + "  ");
			}
		}
	}
}


