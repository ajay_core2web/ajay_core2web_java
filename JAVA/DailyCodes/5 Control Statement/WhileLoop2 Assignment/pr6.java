// WAPTP the product of digits in a given number (234)
//
class P6{
	public static void main(String[] args){
		int num = 234;
		int rem = 0;
		int product = 1;
		while (num>0){
			rem = num%10;
			num = num/10;
			product = rem*product;
		}
		System.out.println(product);
	}
}

			 
