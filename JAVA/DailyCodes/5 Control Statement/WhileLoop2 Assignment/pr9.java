//WAPT calculate the sum of square of odd digits in the given number (2469185)
//
class P9{
	public static void main (String[] args){
		int num = 2469185;
		int rem = 0;
		int sum = 0;
		int squ = 1;
		while (num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==1){
				squ = rem*rem;
				//System.out.print(squ);
			}
			System.out.print(squ);
			sum = sum +squ;
		}
		System.out.println(sum);
	}
}


