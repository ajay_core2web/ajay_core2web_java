// WAPTP the digits from the given numbers which is divisible by 2 (2569185)
//
class P3{
	public static void main(String[] args){
		int num = 2569185;
		int rem = 0;
		while (num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==0){
				System.out.print(rem + "  ");
			}
		}
	}
}

