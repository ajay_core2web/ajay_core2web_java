// WAPTP the cube of even digit of a given number (216985)
//
class P5{
	public static void main(String[] args){
		int num = 216985;
		int rem = 0;
		while (num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==0){
				System.out.print(rem*rem*rem + "  ");
			}
		}
	}
}

