// WAPTP the digit from the given number which is divisible by 2 or 3 (436780521)
//
//
class P4{
	public static void main(String[] args){
		long num = 436780521L;
		long rem = 0;
		while (num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==0 || rem%3==0){
				System.out.print(rem + "  ");
			}
		}
	}
}

