//WAPTP the sum of odd digit and product of even digit in a given number (9367924)
//
class P10{
	public static void main(String[] args){
		int num = 9367924;
		int rem = 0;
		int sum = 0;
		int pro = 1;
		while(num>0){
			rem = num%10;
			num = num/10;
			if (rem%2==1){
				sum = sum + rem;
			}
			else {
				pro = pro*rem;
			}
		}
		System.out.println(sum);
		System.out.println(pro);
	}
}

