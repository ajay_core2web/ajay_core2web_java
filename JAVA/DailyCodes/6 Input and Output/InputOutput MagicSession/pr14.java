import java.util.*;
class P14{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of row : ");
		int row = sc. nextInt();
		System.out.print("Enter the number of column : ");
		int col = sc. nextInt();
		char ch = 'A';
		for (int i=1; i<=row; i++){
			for (int j=1; j<=col; j++){
				System.out.print(ch+" ");
				ch = ((char) (ch+2));
			}
			System.out.println();
		}
	}
}

