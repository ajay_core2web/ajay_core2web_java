// number is present in the table of 16 or not 

import java.util.*;
class P5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc. nextInt();
		if (num%16==0){
			System.out.println(num + " is present in the table of 16");
		}
		else {
			System.out.println(num + " is not present in the table of 16");
		}
	}
}

