// WAP to take a number as input from the user and print the reverse table of it
//
import java.util.*;
class P7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc.nextInt();
		for (int i=10; i>=1; i--){
			System.out.println(i*num);
		}
	}
}

