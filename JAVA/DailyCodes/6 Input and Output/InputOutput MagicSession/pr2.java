//check voter age is eligible for voting or not (age should be positive)

import java.util.*;
class P2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your age : ");
		float age = sc. nextFloat();
		if (age>0 && age>18){
			System.out.println("Eligible for Voting");
		}
		else {
			System.out.println("Not Eligible for Voting");
		}
	}
}

