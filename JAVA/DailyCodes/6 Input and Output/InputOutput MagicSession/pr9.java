// WAP to take range from the user and print the even number 
//
import java.util.*;
class P9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num1 : ");
		int num1 = sc. nextInt();
		System.out.print("Enter num2 : ");
		int num2 = sc.nextInt();
		for (int i=num1; num1<=num2; num1++){
			if (num1%2==0){
				System.out.println(num1);
			}
		}
	}
}

