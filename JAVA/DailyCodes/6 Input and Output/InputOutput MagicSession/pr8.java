//WAP to take as input from the user qnd print the sum of numbers in the range (range => 5 to 7)
//
import java.util.*;
class P8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num1 : ");
		int num1 = sc.nextInt();
		System.out.print("Enter num2 : ");
		int num2 = sc.nextInt();
		int sum = 0;
		for (int i=num1; i<=num2; i++){
			sum = num1+sum;
		}
		System.out.println(sum);
	}
}


