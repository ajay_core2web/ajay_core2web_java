//check whether the given number is even or odd input from user

import java.util.*;
class P1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc. nextInt();
		if (num%2==0){
			System.out.println(num + " is even number");
		}
		else {
			System.out.println(num + " is odd number");
		}
	}
}

