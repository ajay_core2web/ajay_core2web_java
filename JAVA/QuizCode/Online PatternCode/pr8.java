import java.util.*;
class P8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter row : ");
		int row = sc.nextInt();
		char ch = 'A';
		for (int i=1; i<=row; i++){
			for (int j=row-i+1; j>1; j--){
				if (j%2==0){
					System.out.print(j + " ");

				}
				else {
					System.out.print(ch + " ");
				}
			}
			System.out.println();
		}
	}
}

