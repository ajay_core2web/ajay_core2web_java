import java.util.*;
class P9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter array size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];
		for (int i=0; i<arr.length; i++){
			System.out.print("Enter element : ");
			arr[i] = sc.nextInt();
		}
		int sum = 0;
		for (int i=0; i<arr.length; i++){
			System.out.print(arr[i] + " ");
			sum = sum + arr[i];
		}
		System.out.println();

		System.out.println(sum);

	}
}

