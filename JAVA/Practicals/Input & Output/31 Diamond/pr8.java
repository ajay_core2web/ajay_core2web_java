//     A			A
//   B A B		      B A B
// C B A B C 	            C B A B C
//   B A B		  D C B A B C D
//     A		    C B A B C
// 			      B A B
// 			        A
//
import java.util.*;
class P8{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter row : ");
                int row = sc.nextInt();
                int col = 0;
                int space = 0;      
                for (int i=1; i<row*2; i++){
                        int num = 64+i;
                        if (i<=row){
                                space = row-i;
                                col = i*2-1;
                        }
                        else {
                                space = i-row;
                                col = col-2;
                        }
                        for (int sp=1; sp<=space; sp++){
                                System.out.print("\t");
                        }
                        for (int j=1; j<=col; j++){
                             
                                if(j<i){
                                        System.out.print((char)(num--)+"\t");
                                }
                                else {
                                        System.out.print((char)(num++)+"\t");
                                }
                       	}
			System.out.println();
		}
	}
}

