//     C			D
//   B B B		      C C C
// A A A A A		    B B B B B
//   B B B		  A A A A A A A
//     C		    B B B B B 
// 			      C C C
// 			        D
import java.util.*;
class P9{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter row : ");
                int row = sc.nextInt();
                int col = 0;
                int space = 0;
		int num = 64+row;
		int temp = num;
                for (int i=1; i<row*2; i++){
                        if (i<=row){
                                space = row-i;
                                col = i*2-1;
				temp = num--;
                        }
                        else {
                                space = i-row;
                                col = col-2;
				temp = num++;
                        }
                        for (int sp=1; sp<=space; sp++){
                                System.out.print("\t");
                        }
                        for (int j=1; j<=col; j++){
				System.out.print((char)(temp)+"\t");

                        }
                        System.out.println();
                }
        }
}

                                         
