//      1 			            1  
//   2  3  4			        2   3   4
//5  6  7  8  9			     5  6   7   8   9
//   10 11 12			10  11  12  13  14  15  16 
//      13			    17  18  19  20  21 
//				        22  23  24
//				            25
import java.util.*;
class P2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter row : ");
		int row = sc.nextInt();
		int col = 0; 
		int space = 0;
		int num = 1;
		for (int i=1; i<row*2; i++){
			if (i<=row){
				space = row-i;
			}
			else {
				space = i-row;
			}
			for (int sp=1; sp<=space; sp++){
				System.out.print("\t");
			}
			if (i<=row){
				col = i*2-1;
			}
			else {
				col = col-2;
			}
			for (int j=1; j<=col; j++){
				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}

