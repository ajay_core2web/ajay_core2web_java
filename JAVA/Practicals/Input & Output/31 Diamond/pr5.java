//     3	         	4
//   2 2 2  		      3 3 3
// 1 1 1 1 1	            2 2 2 2 2 2
//   2 2 2		  1 1 1 1 1 1 1
//     3		    2 2 2 2 2 
// 		              3 3 3
// 		                4
//
import java.util.*;
class P5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter row : ");
		int row = sc.nextInt();
		int space = 0;
		int col = 0;
		int num = row+1;
		int temp = num;
		for (int i=1; i<row*2; i++){
			if (i<=row){
				space = row-i;
				col = i*2-1;
				temp = num--;
			}
			else {
				space = i-row;
				col = col-2;
				temp = num++;
			}
			for (int sp=1; sp<=space; sp++){
				System.out.print("\t");
			}
			for (int j=1; j<=col; j++){
				System.out.print(num +"\t");
			}
			System.out.println();
		}
	}
}

