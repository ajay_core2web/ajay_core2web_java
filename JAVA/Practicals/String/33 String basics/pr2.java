// WAp to concat the String given by the user and count the length of string after concatenation
//
import java.util.*;
class P2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter str1 and str2 :");
		String str1 = sc.next();
		String str2 = sc.next();
	  	String str3 = str1 + str2;	// String str3 = str1.concat(str2);

		System.out.println(str3);
		System.out.println(str3.length());
	}
}

