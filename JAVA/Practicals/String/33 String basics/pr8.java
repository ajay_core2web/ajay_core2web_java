// WAP to remove void space in the string 
// input  => "  Hello,World  "
// output => Hello,World
//
import java.util.*;
class P8{
	public static void main(String []args){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter string : ");
		String str = sc.next();

		System.out.println(str.trim());
	}
}


