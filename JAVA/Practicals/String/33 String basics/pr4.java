// Take two input string str1 and str2 from the user and print 0 if both strings are equals else print the difference between unequal element 
//
import java.util.*;
class P4{
	public static void main(String []args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter str1 and str2 : ");
		String str1 = sc.next();
		String str2 = sc.next();

		if (str1==str2){
			System.out.println(" 0 ");
		}
		else {
			System.out.println(str1.compareTo(str2));
		}
	}
}

