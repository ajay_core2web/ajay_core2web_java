// WAP to take input as a string from the user and print all the character one by one
//
import java.util.*;
class P3{
	public static void main(String []args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter string : ");
		String str = sc.next();
		
		for (int i=0; i<str.length(); i++){
			System.out.println(str.charAt(i));
		}
	}
}

